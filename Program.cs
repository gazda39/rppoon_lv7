using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LV7
{
    class Program
    {
        static void Main(string[] args)
        {

            //1. zadatak
            double[] array = new double[] { 14, 36, -74, 0, -14, 15, 456, 21.2, 14.1, -4.4, 309};
            NumberSequence sequence = new NumberSequence(array);
            //BubbleSort bubble = new BubbleSort();
            //sequence.SetSortStrategy(bubble);

            //Console.WriteLine("Nesortirani niz:\n" + sequence.ToString());

            //sequence.Sort();
            //Console.WriteLine("Sortirani niz:\n" + sequence.ToString());

            //2. zadatak
            LinearSearch linearSearch = new LinearSearch();
            sequence.SetSearchStrategy(linearSearch);
            Console.WriteLine(sequence.Search(14));

        }
    }
}
